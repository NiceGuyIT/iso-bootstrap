#!/usr/bin/env bash

yarn add bootstrap node-sass
gawk -- '/^@import/ {gsub(/(@import ")/, "\t&_", $0);}; /^$/ {print "\ndiv.iso-bootstrap {"}; {print}; END {print "}"}' node_modules/bootstrap/scss/bootstrap.scss > iso-bootstrap.min.scss
yarn node-sass --output-style compressed --output . --include-path node_modules/bootstrap/scss/ iso-bootstrap.min.scss

