# Isolate Bootstrap
Isolate Bootstrap by prefixing it with a selector so that you can use Bootstrap on part of a web page. This comes in handy if you want to style new content on an existing site that you don't have control over.


## Rabbit Hole
The initial search led me to [Prefixing all selectors for twitter bootstrap in less] and [Applying CSS styles only to certain elements], which led to the [Isolate-Bootstrap-4.1-CSS-Themes] and [Isolated-Bootstrap] repos. The latter repo led me to the blog [How to Isolate Bootstrap CSS to Avoid Conflicts] which contained the instructions on how to build the CSS with your prefix.

[Prefixing all selectors for twitter bootstrap in less]: https://stackoverflow.com/questions/11149266/prefixing-all-selectors-for-twitter-bootstrap-in-less
[How to Isolate Bootstrap CSS to Avoid Conflicts]: https://formden.com/blog/isolate-bootstrap
[Applying CSS styles only to certain elements]: https://stackoverflow.com/questions/11831346/applying-css-styles-only-to-certain-elements
[Isolated-Bootstrap]: https://github.com/toert/Isolated-Bootstrap
[Isolate-Bootstrap-4.1-CSS-Themes]: https://github.com/cryptoapi/Isolate-Bootstrap-4.1-CSS-Themes


## Why reinvent the wheel?
Hosting a production site based on someone else's repo means you're relying on them to keep the repo available. If they delete the repo or make it private, your site breaks. This is specifically for the sites I host.


## Generate
This works on Linux, might work on macOS and probably doesn't work on Windows.

```bash
yarn add bootstrap node-sass
gawk -- '/^@import/ {gsub(/(@import ")/, "\t&_", $0);}; /^$/ {print "\ndiv.iso-bootstrap {"}; {print}; END {print "}"}' node_modules/bootstrap/scss/bootstrap.scss > iso-bootstrap.min.scss
yarn node-sass --output-style compressed --output . --include-path node_modules/bootstrap/scss/ iso-bootstrap.min.scss
```

## Hosting
Host this on your own GitLab Page to make the CSS available for distribution. Using the CSS file from your repo will return an incorrect content type of `content-type: text/plain; charset=utf-8`. Using the CSS file from your GitLab Page will return the correct content type `content-type: text/css; charset=utf-8`.

 1. Create your own repo, or fork this one.
 2. Copy `iso-bootstrap.min.scss` to `public/css/iso-bootstrap-v0.0.0.min.scss`
 3. Copy `.gitlab-ci.yml` to your repo.
 4. Optionally, copy `public/index.html` and `public/css/style.css` to your repo. I'm not sure if this is needed, but it creates a generic HTML page for your GitLab Page.
 5. Commit your changes and the GitLab CI/CD will create your site at `user.gitlab.io/repo`.

## Usage
Add this to the `HEAD` of the site.
```html
<link rel="stylesheet" href="https://niceguyit.gitlab.io/iso-bootstrap/css/iso-bootstrap-v4.5.2.min.css.gz">
```

Add the `iso-bootstrap` class to a `div` and everything in the `div` will have bootstrap available.
```html
<div class="iso-bootstrap">
  <!-- Your code -->
</div>
```

